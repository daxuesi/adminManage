package com.ljj.admin;

import org.junit.jupiter.api.Test;
import org.springframework.util.ClassUtils;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamTests {


    private static<T> void show(String title,Stream<T> stream)
    {
        final int SIZE=10;
        List<T> list=stream.limit(SIZE+1).collect(Collectors.toList());
        System.out.println(title +":");

        for(int i=0; i<list.size();i++)
        {
            if(i>0)  System.out.println(",");
            if(i<SIZE) System.out.println(list.get(i));
            else System.out.println("——————————————————————");
        }
    }


    @Test
    void StreamCreate() throws IOException {
        Path path= Paths.get("E://home//admin//logs//sys-info.log");
        String content=new String(Files.readAllBytes(path), StandardCharsets.UTF_8);

        Stream<String> stringStream=Stream.of(content.split("\\PL+"));

//        show("content",stringStream);

        Stream<String> stringStream1=Stream.generate(()->"Echo");

//        show("Echo",stringStream1);


        Stream<Double> doubleStream=Stream.generate(Math :: random);

        show("random",doubleStream);



    }


}
