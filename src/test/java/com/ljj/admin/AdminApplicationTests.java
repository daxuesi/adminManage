package com.ljj.admin;

import com.ljj.admin.mybatis.system.beans.SysRole;
import com.ljj.admin.mybatis.system.mapper.SysRoleDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class AdminApplicationTests {


	@Autowired
	private SysRoleDAO sysRoleDAO;


	@Test
	void contextLoads() {

		Set<String> permsSet = new HashSet<>();
		List<SysRole> sysRoles = sysRoleDAO.selectRoleKeyByUserId(117l);
		Set<String> collect = sysRoles.stream().map(p -> {
			return p.getRoleKey();
		}).collect(Collectors.toSet());
		permsSet.addAll(collect);

	}

}
