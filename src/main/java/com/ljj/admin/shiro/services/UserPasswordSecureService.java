package com.ljj.admin.shiro.services;

import com.ljj.admin.mybatis.system.beans.SysUser;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 用户登录密码
 */

@Service
public class UserPasswordSecureService  {

    private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();

    @Value("${shiro.password.algorithmName}")
    private String algorithmName;
    @Value("${shiro.password.hashIterations}")
    private int hashIterations;


    /**
     * 获取用户加密密码
     * @param sysUser
     * @return
     */
    public SysUser getEncryptPasswordUserInfo(SysUser sysUser)
    {
        // 生成随机盐
        sysUser.setSalt(randomNumberGenerator.nextBytes(3).toHex());

        String encryptPass=new SimpleHash(
                algorithmName,
                sysUser.getPassword(),
                ByteSource.Util.bytes(sysUser.getSalt()),
                hashIterations
                ).toHex();

        sysUser.setPassword(encryptPass);
        return sysUser;
    }

}
