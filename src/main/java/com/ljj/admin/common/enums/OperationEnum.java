package com.ljj.admin.common.enums;

import com.fasterxml.jackson.databind.util.EnumValues;

public enum OperationEnum  {

    //其他操作
    OTHER(0,"其他") ,

    /**
     * 后台用户
     */
    MANAGE(1,"后台用户"),


    /**
     * 手机端用户
     */
    MOBILE(2,"手机端用户")

    ;

    private int value;
    private String label;
    OperationEnum(int value, String label) {
        this.value=value;
        this.label=label;
    }

}
