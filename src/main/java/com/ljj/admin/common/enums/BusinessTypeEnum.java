package com.ljj.admin.common.enums;

/**
 * 业务类型
 */
public enum BusinessTypeEnum {

    /**
     * 其他
     */
    OTHER,

    // 新增
    INSERT,

    //修改
    UPDATE,

    // 删除
    DELETE,

    // 授权
    GRANT,

    // 导出
    EXPORT,

    //导入
    IMPORT,

    //强退
    FORCE,

    //清空
    CLEAR,


}
