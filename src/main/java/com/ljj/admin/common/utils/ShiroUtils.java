package com.ljj.admin.common.utils;

import cn.hutool.core.bean.BeanUtil;
import com.ljj.admin.mybatis.system.beans.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.util.ObjectUtils;

/**
 * shiro 工具类
 */
public class ShiroUtils {

    private ShiroUtils(){
        throw new IllegalStateException("Utility class");
    }

    private static Subject getSubject()
    {
        return SecurityUtils.getSubject();
    }

    public static Session getSession()
    {
        return getSubject().getSession();
    }

    public static void logout()
    {
        getSubject().logout();
    }

    public static SysUser getSysUser()
    {
        SysUser sysUser=null;
        Object obj = getSubject().getPrincipal();
        if(!ObjectUtils.isEmpty(obj))
        {
            sysUser = new SysUser();
            BeanUtil.copyProperties(obj, sysUser);
        }
        return sysUser;
    }




}
