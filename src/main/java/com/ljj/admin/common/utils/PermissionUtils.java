package com.ljj.admin.common.utils;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;

/**
 * permission 工具类
 *
 */
public class PermissionUtils {

    private static Logger logger= LoggerFactory.getLogger(PermissionUtils.class);

    private PermissionUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 查看数据的权限
     */
    private static final String VIEW_PERMISSION = "no.view.permission";

    /**
     * 创建数据的权限
     */
    private static final String CREATE_PERMISSION = "no.create.permission";

    /**
     * 修改数据的权限
     */
    private static final String UPDATE_PERMISSION = "no.update.permission";

    /**
     * 删除数据的权限
     */
    private static final String DELETE_PERMISSION = "no.delete.permission";

    /**
     * 导出数据的权限
     */
    private static final String EXPORT_PERMISSION = "no.export.permission";

    /**
     * 其他数据的权限
     */
    private static final String PERMISSION = "no.permission";

    /**
     * 权限错误消息提醒
     *
     * @param permissionsStr 错误信息
     * @return 提示信息
     */
    public static String getMsg(String permissionsStr) {

        StringBuilder builder=new StringBuilder("[");
        builder.append(permissionsStr).append("]");
        return builder.toString();
    }

    /**
     * 返回用户属性值
     *
     * @param property 属性名称
     * @return 用户属性值
     */
    public static Object getPrincipalProperty(String property) {
        Subject subject = SecurityUtils.getSubject();
        if (ObjectUtil.isNotNull(subject)) {
            Object principal = subject.getPrincipal();
            try {
                BeanInfo bi = Introspector.getBeanInfo(principal.getClass());
                for (PropertyDescriptor pd : bi.getPropertyDescriptors()) {
                    if (pd.getName().equals(property)) {
                        return pd.getReadMethod().invoke(principal, (Object[]) null);
                    }
                }
            } catch (Exception e) {

                logger.error("Error reading property [{}] from principal of type [{}]", property,
                        principal.getClass().getName());
            }
        }
        return null;
    }


}
