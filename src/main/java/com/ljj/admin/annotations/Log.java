package com.ljj.admin.annotations;

import com.ljj.admin.common.enums.BusinessTypeEnum;
import com.ljj.admin.common.enums.OperationEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Log {

    String value() default "";

    BusinessTypeEnum businessType() default BusinessTypeEnum.OTHER;
}
