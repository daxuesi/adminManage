package com.ljj.admin.filter.xss;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.servlet.DispatcherType;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class XssFilterConfig {


    @Value("${xss.enabled}")
    private String enabled;

    @Value("${xss.excludes}")
    private String excludes;

    @Value("${xss.urlPatterns}")
    private String urlPatterns;


    @Bean
    public FilterRegistrationBean xssFilterRegistration()
    {
        FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean();
        filterRegistrationBean.setDispatcherTypes(DispatcherType.REQUEST);
        filterRegistrationBean.setFilter(new XssFilter());
        filterRegistrationBean.addUrlPatterns(StringUtils.split(urlPatterns,","));
        filterRegistrationBean.setName("XssFilter");
        filterRegistrationBean.setOrder(9999);
        Map<String,String > initParameters=new HashMap<>();
        initParameters.put("excludes",excludes);
        initParameters.put("enabled",enabled);
        filterRegistrationBean.setInitParameters(initParameters);
        return filterRegistrationBean;
    }





}
