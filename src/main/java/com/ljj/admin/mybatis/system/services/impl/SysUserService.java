package com.ljj.admin.mybatis.system.services.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.beans.SysUserPostKey;
import com.ljj.admin.mybatis.system.beans.SysUserRoleKey;
import com.ljj.admin.mybatis.system.mapper.SysUserDAO;
import com.ljj.admin.mybatis.system.mapper.SysUserPostDAO;
import com.ljj.admin.mybatis.system.mapper.SysUserRoleDAO;
import com.ljj.admin.mybatis.system.services.ISysUserService;
import com.ljj.admin.shiro.services.UserPasswordSecureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 用户登录 逻辑处理层
 */

@Service
public class SysUserService implements ISysUserService {

    @Autowired
    private SysUserDAO sysUserDAO;

    @Autowired
    private SysUserRoleDAO sysUserRoleDAO;

    @Autowired
    private SysUserPostDAO sysUserPostDAO;

    @Autowired
    private UserPasswordSecureService secureService;

    // 用户登录函数
    public SysUser userLoginOn(String username, String password) {
        SysUser sysUser = sysUserDAO.selectByUserName(username);

        return sysUser;
    }

    public SysUser selectByUserName(String username) {
        SysUser sysUser = sysUserDAO.selectByUserName(username);
        return sysUser;
    }

    @Override
    public int updateByPrimaryKeySelective(SysUser record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(SysUser record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(SysUser record) {
        return 0;
    }

    @Override
    public List<SysUser> selectNormalUserList(SysUser sysUser) {
        return null;
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(SysUser record) {
        return 0;
    }

    @Override
    public int insertSelective(SysUser record) {
        return 0;
    }

    public SysUser selectByPrimaryKey(Long id)
    {
        return sysUserDAO.selectByPrimaryKey(id);
    }

    public List<SysUser> GetAllUserInfo(SysUser sysUser) {
        return sysUserDAO.selectNormalUserList(sysUser);
    }


    public int checkLoginNameUnique(String loginName) {
        return sysUserDAO.checkLoginNameUnique(loginName);
    }

    @Override
    public List<SysUser> selectByDeptId(Long deptId) {
        return null;
    }

    /**
     * 添加用户信息
     *
     * @param sysUser
     * @return
     */
    public int insertUserInfo(SysUser sysUser) {
        sysUser.setCreateTime(DateTime.now());
        sysUser.setSex("0");
        sysUser.setCreateBy(ShiroUtils.getSysUser().getLoginName());
        sysUser.setUserType("00");
        sysUser.setLoginDate(DateTime.now());
        sysUser.setUpdateTime(DateTime.now());
        sysUser.setUpdateBy(ShiroUtils.getSysUser().getLoginName());

        int rowNum = -1;
        rowNum = sysUserDAO.insertSelective(sysUser);
        // 用户-岗位信息操作
        rowNum +=updateUserRoleAndPostInfo(sysUser);
        return rowNum;
    }


    /***
     * 删除用户信息
     * @param sysUser
     * @return
     */
    public int delUserInfoByUser(SysUser sysUser) {
        int rowNum = -1;
        if (ObjectUtil.isAllEmpty(sysUser)) {
            return rowNum;
        }
        // 删除用户- 岗位信息表
        rowNum += sysUserPostDAO.deleteByUserId(sysUser);
        rowNum += sysUserRoleDAO.deleteByUserId(sysUser);
        rowNum += sysUserDAO.deleteByPrimaryKey(sysUser.getUserId());
        return rowNum;
    }


    /**
     * 更新用户信息
     * @param sysUser
     * @return
     */
    public int updateUserInfoByUser(SysUser sysUser)
    {
        int rowNum=-1;
        rowNum +=updateUserRoleAndPostInfo(sysUser);
        rowNum +=sysUserDAO.updateByPrimaryKeySelective(sysUser);
        return rowNum;
    }


    /**
     * 更新用户岗位 和 角色信息
     * @param sysUser
     * @return
     */
    private int updateUserRoleAndPostInfo(SysUser sysUser)
    {
        int rowNum=-1;
        if (ObjectUtil.isAllEmpty(sysUser)) {
            return rowNum;
        }
        // 用户-岗位信息操作
        if (sysUser.getPostIds().length > 0) {
            //删除 当前 用户所对应的 用户和岗位信息
            rowNum += sysUserPostDAO.deleteByUserId(sysUser);
            //新增 用户岗位对应信息表
            for (long postId : sysUser.getPostIds()) {
                SysUserPostKey sysUserPostKey = new SysUserPostKey();
                sysUserPostKey.setUserId(sysUser.getUserId());
                sysUserPostKey.setPostId(postId);
                rowNum += sysUserPostDAO.insert(sysUserPostKey);
            }

        }
        // 用户-角色信息操作
        if (sysUser.getRoleIds().length > 0) {
            rowNum += sysUserRoleDAO.deleteByUserId(sysUser);
            for (Long roldId : sysUser.getRoleIds()) {
                SysUserRoleKey sysUserRoleKey = new SysUserRoleKey();
                sysUserRoleKey.setUserId(sysUser.getUserId());
                sysUserRoleKey.setRoleId(roldId);
                rowNum += sysUserRoleDAO.insert(sysUserRoleKey);
            }

        }

        return rowNum;
    }


    /**
     * 重置用户密码
     * @param userId
     * @return
     */
    public int resetUserPwdByUserId(Long userId)
    {
        int rowNum=0;

        SysUser sysUser = sysUserDAO.selectByPrimaryKey(userId);
        if(ObjectUtil.isNotNull(sysUser))
        {
            //重置密码 为 123
            sysUser.setPassword("123");
            SysUser UserInfo = secureService.getEncryptPasswordUserInfo(sysUser);

            // 更新用户信息
            rowNum = sysUserDAO.updateByPrimaryKeySelective(UserInfo);
        }


        return rowNum;
    }


}
