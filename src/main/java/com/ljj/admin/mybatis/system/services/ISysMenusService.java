package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysMenu;
import com.ljj.admin.mybatis.system.beans.SysRoleMenuKey;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.web.bean.common.DTree;

import java.util.List;

public interface ISysMenusService {
    int deleteByPrimaryKey(Long menuId);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(Long menuId);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKeyWithBLOBs(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

    List<SysMenu> selectMenuAll();

    List<SysMenu> selectMenuList();

    List<SysMenu> selectByParentId(long menuId);

    List<SysMenu> selectMenuPermsByUserId(Long userId);

    List<SysMenu> getMenusAll(SysUser sysUser);

    List<DTree> GetSysMenuDTreeByUser(long roleId);

    List<SysRoleMenuKey> selectByMenuId(long menuId);

    List<SysMenu> GetSysMenuListByUser(SysUser sysUser);
}
