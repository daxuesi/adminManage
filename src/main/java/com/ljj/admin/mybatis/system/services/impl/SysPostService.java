package com.ljj.admin.mybatis.system.services.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import com.ljj.admin.mybatis.system.beans.SysPost;
import com.ljj.admin.mybatis.system.beans.SysUserPostKey;
import com.ljj.admin.mybatis.system.mapper.SysPostDAO;
import com.ljj.admin.mybatis.system.mapper.SysRoleMenuDAO;
import com.ljj.admin.mybatis.system.mapper.SysUserPostDAO;
import com.ljj.admin.mybatis.system.services.ISysPostService;
import com.ljj.admin.web.bean.common.CheckArr;
import com.ljj.admin.web.bean.common.DTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SysPostService implements ISysPostService {

    @Autowired
    private SysPostDAO sysPostDAO;


    @Autowired
    private SysUserPostDAO sysUserPostDAO;


    @Override
    public int deleteByPrimaryKey(Long postId) {
        return 0;
    }

    @Override
    public int insert(SysPost record) {
        return 0;
    }

    @Override
    public int insertSelective(SysPost record) {
        return 0;
    }

    @Override
    public SysPost selectByPrimaryKey(Long postId) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(SysPost record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(SysPost record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(SysPost record) {
        return 0;
    }

    /**
     * 获取所有的岗位信息
     * @return
     */
    public List<SysPost> selectAllPostInfo()
    {
        return sysPostDAO.selectAllPostInfo();
    }

    public List<SysPost> selectAllPostInfoByUserId(long userId)
    {
        List<SysPost> sysPosts = sysPostDAO.selectAllPostInfo();
        List<SysUserPostKey> sysUserPostKeys = sysUserPostDAO.selectByUserId(userId);
        if(CollectionUtil.isNotEmpty(sysPosts))
        {
            for(SysPost sysPost:sysPosts)
            {
                if(CollectionUtil.isNotEmpty(sysUserPostKeys))
                {
                    long userPostCount = sysUserPostKeys.stream().filter(p -> p.getUserId() == userId && p.getPostId() == sysPost.getPostId()).count();
                    if(userPostCount>0)
                    {
                        sysPost.setFlag(true);
                    }
                }
            }

        }


        return sysPosts;
    }



    /**
     * 根据postId 获取 岗位信息
     * @return
     */
    public SysPost selectByPostId(Long postId)
    {
        return sysPostDAO.selectByPrimaryKey(postId);
    }

    public List<DTree> getPostDtree()
    {
        List<DTree> dTrees =new ArrayList<>();
        List<SysPost> sysPosts = sysPostDAO.selectAllPostInfo();
        if(CollectionUtil.isNotEmpty(sysPosts))
        {
            for(SysPost sysPost:sysPosts) {
                DTree dTree = new DTree();
                CheckArr checkArr=new CheckArr();
                dTree.setCheckArr(checkArr);
                dTree.setId(sysPost.getPostId().toString());
                dTree.setTitle(sysPost.getPostName());
                dTree.setParentId("0");
                dTree.setLast(false);
                dTrees.add(dTree);
            }
        }
        return dTrees;
    }


    /**
     * 根据用户ID 获取岗位树
     * @param userId
     * @return
     */
    public List<DTree> getPostDTreeByUserId(Long userId)
    {
        List<DTree> dTrees =new ArrayList<>();
        List<SysPost> sysPosts = sysPostDAO.selectAllPostInfo();
        //根据 用户Id 获取 用户信息
        List<SysUserPostKey> sysUserPostKeys = sysUserPostDAO.selectByUserId(userId);
        if(CollectionUtil.isNotEmpty(sysPosts))
        {
            for(SysPost sysPost:sysPosts) {
                DTree dTree = new DTree();
                CheckArr checkArr=new CheckArr();
                dTree.setCheckArr(checkArr);
                dTree.setId(sysPost.getPostId().toString());
                dTree.setTitle(sysPost.getPostName());
                dTree.setParentId("0");
                dTree.setLast(false);
                if(CollectionUtil.isNotEmpty(sysUserPostKeys))
                {
                    long postCountById = sysUserPostKeys.stream().filter(p -> p.getUserId() == userId && p.getPostId() == sysPost.getPostId()).count();
                    if(postCountById>0)
                    {
                        CheckArr checkPost=new CheckArr();
                        checkPost.setChecked("1");
                        dTree.setCheckArr(checkPost);
                    }
                }
                dTrees.add(dTree);
            }
        }
        return dTrees;
    }


    /**
     * 新增
     * @param sysPost
     * @return
     */
    public int insertSysPost(SysPost sysPost)
    {
        int rowNum=0;
        sysPost.setCreateTime(DateTime.now());
        sysPost.setUpdateTime(DateTime.now());
        rowNum=sysPostDAO.insertSelective(sysPost);
        return rowNum;
    }


    /**
     * 更新
     * @param sysPost
     * @return
     */
    public int updateSysPost(SysPost sysPost)
    {
        int rowNum=0;
        sysPost.setUpdateTime(DateTime.now());

        rowNum = sysPostDAO.updateByPrimaryKeySelective(sysPost);
        return rowNum;
    }








}
