package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysDept;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.web.bean.common.DTree;

import java.util.List;

public interface ISysDeptService {

    int deleteByPrimaryKey(Long deptId);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    SysDept selectByPrimaryKey(Long deptId);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);

    List<SysDept> getSysDeptList();

    List<SysDept> getSysDeptListByParentId(Long parentId);

    List<DTree> getDeptDtree();

    List<SysDept> getDeptListAll();

    int insertDept(SysDept sysDept);

    int updateDept(SysDept sysDept);

    List<SysUser> getSysUserInfoByDeptId(Long deptId);

    int delByDeptId(Long deptId);

}
