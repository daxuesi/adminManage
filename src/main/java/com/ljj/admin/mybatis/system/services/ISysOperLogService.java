package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysOperLog;
import com.ljj.admin.mybatis.system.beans.SysOperLogWithBLOBs;

public interface ISysOperLogService {
    int deleteByPrimaryKey(Long operId);

    int insert(SysOperLogWithBLOBs record);

    int insertSelective(SysOperLogWithBLOBs record);

    SysOperLogWithBLOBs selectByPrimaryKey(Long operId);

    int updateByPrimaryKeySelective(SysOperLogWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SysOperLogWithBLOBs record);

    int updateByPrimaryKey(SysOperLog record);
}
