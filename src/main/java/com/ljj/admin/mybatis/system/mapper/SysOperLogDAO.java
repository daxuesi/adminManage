package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysOperLog;
import com.ljj.admin.mybatis.system.beans.SysOperLogWithBLOBs;
import org.springframework.stereotype.Repository;

@Repository
public interface SysOperLogDAO {
    int deleteByPrimaryKey(Long operId);

    int insert(SysOperLogWithBLOBs record);

    int insertSelective(SysOperLogWithBLOBs record);

    SysOperLogWithBLOBs selectByPrimaryKey(Long operId);

    int updateByPrimaryKeySelective(SysOperLogWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(SysOperLogWithBLOBs record);

    int updateByPrimaryKey(SysOperLog record);
}