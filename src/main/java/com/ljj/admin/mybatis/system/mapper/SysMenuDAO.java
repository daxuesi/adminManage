package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysMenu;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysMenuDAO {
    int deleteByPrimaryKey(Long menuId);

    int insert(SysMenu record);

    int insertSelective(SysMenu record);

    SysMenu selectByPrimaryKey(Long menuId);

    int updateByPrimaryKeySelective(SysMenu record);

    int updateByPrimaryKeyWithBLOBs(SysMenu record);

    int updateByPrimaryKey(SysMenu record);

    List<SysMenu> selectMenuAll();

    List<SysMenu> selectMenuList();

    List<SysMenu> selectByParentId(long menuId);

    List<SysMenu> selectMenuPermsByUserId(Long userId);


}