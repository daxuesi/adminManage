package com.ljj.admin.mybatis.system.services.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.mybatis.system.beans.SysDept;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.mapper.SysDeptDAO;
import com.ljj.admin.mybatis.system.mapper.SysUserDAO;
import com.ljj.admin.mybatis.system.services.ISysDeptService;
import com.ljj.admin.web.bean.common.CheckArr;
import com.ljj.admin.web.bean.common.DTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysDeptService implements ISysDeptService {

    @Autowired
    private SysDeptDAO sysDeptDAO;


    @Autowired
    private SysUserDAO sysUserDAO;



    public List<DTree> getDeptDtree()
    {
        List<DTree> dTrees=new ArrayList<>();
        List<SysDept> sysDeptList = sysDeptDAO.getSysDeptList();
        if(CollectionUtil.isNotEmpty(sysDeptList))
        {
            for(SysDept sysDept : sysDeptList)
            {
                DTree dTree=new DTree();
                CheckArr checkArr=new CheckArr();
                dTree.setCheckArr(checkArr);
                dTree.setParentId(sysDept.getParentId().toString());
                dTree.setId(sysDept.getDeptId().toString());
                dTree.setTitle(sysDept.getDeptName());
                dTree.setSpread(true);
                dTrees.add(dTree);
            }
        }
        return dTrees;
    }


    @Override
    public int deleteByPrimaryKey(Long deptId) {
        return 0;
    }

    @Override
    public int insert(SysDept record) {
        return 0;
    }

    @Override
    public int insertSelective(SysDept record) {
        return 0;
    }

    /**
     * 根据Id 取当前 部门值
     * @param deptId
     * @return
     */
    public SysDept selectByPrimaryKey(Long deptId)
    {
        return sysDeptDAO.selectByPrimaryKey(deptId);
    }

    @Override
    public int updateByPrimaryKeySelective(SysDept record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(SysDept record) {
        return 0;
    }

    @Override
    public List<SysDept> getSysDeptList() {
        return null;
    }


    /**
     * 返回 删除标识为 0 的部门列表
     * @return
     */
    public List<SysDept> getDeptListAll()
    {
        return sysDeptDAO.getSysDeptList();
    }


    /**
     * 新增部门信息
     * @param sysDept
     * @return
     */
    public int insertDept(SysDept sysDept)
    {
        int rowNum=0;
        // 根据parentId 获取父部门信息
        SysDept parentDept = sysDeptDAO.selectByPrimaryKey(sysDept.getParentId());
        if(ObjectUtil.isNull(parentDept))
        {
            return -1;
        }

        String ancestors=parentDept.getAncestors()+","+sysDept.getParentId();
        sysDept.setAncestors(ancestors);
        sysDept.setCreateTime(DateTime.now());
        sysDept.setUpdateTime(DateTime.now());
        sysDept.setDelFlag("0");
        rowNum= sysDeptDAO.insertSelective(sysDept);
        return rowNum;

    }


    /**
     * 更新部门信息
     * @param sysDept
     * @return
     */
    public int updateDept(SysDept sysDept)
    {
        int rowNum=0;
        // 获取上级部门信息
        SysDept parentDept = sysDeptDAO.selectByPrimaryKey(sysDept.getParentId());
        String ancestors=parentDept.getAncestors()+","+sysDept.getParentId();
        sysDept.setAncestors(ancestors);
        sysDept.setUpdateTime(DateTime.now());
        return sysDeptDAO.updateByPrimaryKeySelective(sysDept);
    }


    /**
     * 获取下级部门列表信息
     * @param parentId
     * @return
     */
    public List<SysDept> getSysDeptListByParentId(Long parentId)
    {
        return  sysDeptDAO.getSysDeptListByParentId(parentId);
    }


    /**
     * 获取该部门的所有用户信息
     * @param deptId
     * @return
     */
    public List<SysUser> getSysUserInfoByDeptId(Long deptId){
        return sysUserDAO.selectByDeptId(deptId);
    }


    /**
     * 部门信息删除操作
     * @param deptId
     * @return
     */
    public int delByDeptId(Long deptId)
    {
        return sysDeptDAO.deleteByPrimaryKey(deptId);
    }





}
