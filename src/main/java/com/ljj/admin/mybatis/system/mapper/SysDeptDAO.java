package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysDept;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysDeptDAO {
    int deleteByPrimaryKey(Long deptId);

    int insert(SysDept record);

    int insertSelective(SysDept record);

    SysDept selectByPrimaryKey(Long deptId);

    int updateByPrimaryKeySelective(SysDept record);

    int updateByPrimaryKey(SysDept record);

    List<SysDept> getSysDeptList();

    List<SysDept> getSysDeptListByParentId(Long parentId);

}