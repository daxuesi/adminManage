package com.ljj.admin.mybatis.system.services.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.mybatis.system.beans.SysRole;
import com.ljj.admin.mybatis.system.beans.SysRoleMenuKey;
import com.ljj.admin.mybatis.system.beans.SysUserRoleKey;
import com.ljj.admin.mybatis.system.mapper.SysRoleDAO;
import com.ljj.admin.mybatis.system.mapper.SysRoleMenuDAO;
import com.ljj.admin.mybatis.system.mapper.SysUserRoleDAO;
import com.ljj.admin.mybatis.system.services.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class SysRoleService implements ISysRoleService {

    @Autowired
    private SysRoleDAO sysRoleDAO;

    @Autowired
    private SysRoleMenuDAO sysRoleMenuDAO;

    @Autowired
    private SysUserRoleDAO sysUserRoleDAO;

    @Override
    public int deleteByPrimaryKey(Long roleId) {
        return 0;
    }

    @Override
    public int insert(SysRole record) {
        return 0;
    }

    @Override
    public int insertSelective(SysRole record) {
        return 0;
    }

    @Override
    public SysRole selectByPrimaryKey(Long roleId) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(SysRole record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeyWithBLOBs(SysRole record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(SysRole record) {
        return 0;
    }

    public List<SysRole> selectAllRoleInfo() {
        return sysRoleDAO.selectAllRoleInfo();
    }

    @Override
    public List<SysRole> selectByRoleInfo(SysRole sysRole) {
        return null;
    }


    /**
     * 获取 用户所有权限
     * @param userId
     * @return
     */
    public Set<String> selectRoleKeyByUserId(long userId)
    {

        Set<String> permsSet = new HashSet<>();
        List<SysRole> sysRoles = sysRoleDAO.selectRoleKeyByUserId(userId);
        if(CollectionUtil.isNotEmpty(sysRoles))
        {
            Set<String> roleSet = sysRoles.stream().map(p -> {
                return p.getRoleKey();
            }).collect(Collectors.toSet());
            permsSet.addAll(roleSet);
        }

        return permsSet;
    }


    public List<SysRole> selectAllRoleInfoByUserId(Long userId)
    {
        List<SysRole> sysRoles = sysRoleDAO.selectAllRoleInfo();
        List<SysUserRoleKey> sysUserRoleKeys = sysUserRoleDAO.selectByUserId(userId);
        if(CollectionUtil.isNotEmpty(sysRoles))
        {
            for (SysRole sysRole :sysRoles)
            {
                if(CollectionUtil.isNotEmpty(sysUserRoleKeys))
                {
                    long userRoleCount = sysUserRoleKeys.stream().filter(p -> p.getRoleId() == sysRole.getRoleId()).count();
                    if(userRoleCount>0)
                    {
                        sysRole.setFlag(true);
                    }
                }
            }
        }

        return  sysRoles;
    }



    /**
     * 根据roleId 获取权限信息
     *
     * @param roleId
     * @return
     */
    public SysRole selectRoleInfoById(long roleId) {
        return sysRoleDAO.selectByPrimaryKey(roleId);
    }

    /**
     * 保存角色修改信息
     *
     * @param sysRole
     * @return
     */
    public int updateSysRoleInfo(SysRole sysRole) {
        sysRole.setUpdateTime(DateTime.now());
        // 更新role 表
        sysRoleDAO.updateByPrimaryKeySelective(sysRole);

        //删除角色菜单对应表
        sysRoleMenuDAO.deletePrimaryKeyByRoleId(sysRole);
        // 新增对应关系

        int insertRow = 0;
        if (sysRole.getMenuIds().length > 0) {
            List<SysRoleMenuKey> list = new ArrayList<>();
            for (long menuId : sysRole.getMenuIds()) {
                SysRoleMenuKey sysRoleMenuKey = new SysRoleMenuKey();
                sysRoleMenuKey.setRoleId(sysRole.getRoleId());
                sysRoleMenuKey.setMenuId(menuId);
                list.add(sysRoleMenuKey);
            }
            if (CollectionUtil.isNotEmpty(list)) {
                insertRow = sysRoleMenuDAO.batchInsertRoleMenu(list);
            }
        }
        return insertRow;
    }


    /**
     * 新增用户信息
     *
     * @param sysRole
     * @return
     */
    public int insertSysRoleInfo(SysRole sysRole) {

        int rowNum = 0;
        sysRoleDAO.insertSelective(sysRole);
        if (sysRole.getMenuIds().length > 0) {
            List<SysRoleMenuKey> list = new ArrayList<>();
            for (long menuId : sysRole.getMenuIds()) {
                SysRoleMenuKey sysRoleMenuKey = new SysRoleMenuKey();
                sysRoleMenuKey.setRoleId(sysRole.getRoleId());
                sysRoleMenuKey.setMenuId(menuId);
                list.add(sysRoleMenuKey);
            }
            if (CollectionUtil.isNotEmpty(list)) {
                rowNum = sysRoleMenuDAO.batchInsertRoleMenu(list);
            }

        }

        return rowNum;
    }


    /**
     * 根据roleId 删除 权限信息表  多个  roleId   用,分割
     *
     * @param roleIds
     * @return
     */
    public int deleteSysRoleInfo(String roleIds) {
        //
        String[] roleIdArray = roleIds.split(",");
        int rowNum = -1;
        // 1 删除 权限 菜单信息表
        for (String roleId : roleIdArray) {
            Long RoleIdL = Long.parseLong(roleId);
            SysRole sysRole = sysRoleDAO.selectByPrimaryKey(RoleIdL);
            if (ObjectUtil.isAllNotEmpty(sysRole)) {
                //删除角色菜单对应表
                rowNum += sysRoleMenuDAO.deletePrimaryKeyByRoleId(sysRole);
                rowNum += sysRoleDAO.deleteByPrimaryKey(RoleIdL);
            }
        }
        return rowNum;
    }


    /**
     * 根据role 信息判断是否已存在
     * 如果存在 返回失败
     *
     * @param sysRole
     * @return
     */
    public boolean checkExitByRoleInfo(SysRole sysRole) {
        List<SysRole> sysRoles = sysRoleDAO.selectByRoleInfo(sysRole);

        return CollectionUtil.isNotEmpty(sysRoles);
    }


}
