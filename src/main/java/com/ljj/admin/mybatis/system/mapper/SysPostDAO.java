package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysPost;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysPostDAO {
    int deleteByPrimaryKey(Long postId);

    int insert(SysPost record);

    int insertSelective(SysPost record);

    SysPost selectByPrimaryKey(Long postId);

    int updateByPrimaryKeySelective(SysPost record);

    int updateByPrimaryKeyWithBLOBs(SysPost record);

    int updateByPrimaryKey(SysPost record);

    List<SysPost> selectAllPostInfo();

}