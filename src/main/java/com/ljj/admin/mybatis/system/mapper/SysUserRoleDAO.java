package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.beans.SysUserRoleKey;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserRoleDAO {

    int deleteByPrimaryKey(SysUserRoleKey key);

    int deleteByUserId(SysUser sysUser);

    int insert(SysUserRoleKey record);

    int insertSelective(SysUserRoleKey record);

    List<SysUserRoleKey> selectByUserId(long userId);
}