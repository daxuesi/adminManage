package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysDictType;

import java.util.List;

public interface ISysDictTypeService {

    int deleteByPrimaryKey(Long dictId);

    int insert(SysDictType record);

    int insertSelective(SysDictType record);

    SysDictType selectByPrimaryKey(Long dictId);

    int updateByPrimaryKeySelective(SysDictType record);

    int updateByPrimaryKey(SysDictType record);

    List<SysDictType> selectAll();
}
