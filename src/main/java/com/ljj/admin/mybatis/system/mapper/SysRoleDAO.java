package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysRole;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleDAO {
    int deleteByPrimaryKey(Long roleId);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long roleId);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKeyWithBLOBs(SysRole record);

    int updateByPrimaryKey(SysRole record);

    List<SysRole> selectAllRoleInfo();

    List<SysRole> selectByRoleInfo(SysRole sysRole);


    List<SysRole> selectRoleKeyByUserId(long userId);


}