package com.ljj.admin.mybatis.system.services.impl;

import com.ljj.admin.mybatis.system.beans.SysOperLogWithBLOBs;
import com.ljj.admin.mybatis.system.mapper.SysOperLogDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysOperLogService {

    @Autowired
    private SysOperLogDAO sysOperLogDAO;

    public
    int insertSelective(SysOperLogWithBLOBs record)
    {
        return sysOperLogDAO.insertSelective(record);
    }








}
