package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysRole;

import java.util.List;
import java.util.Set;

public interface ISysRoleService {

    int deleteByPrimaryKey(Long roleId);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long roleId);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKeyWithBLOBs(SysRole record);

    int updateByPrimaryKey(SysRole record);

    List<SysRole> selectAllRoleInfo();

    List<SysRole> selectByRoleInfo(SysRole sysRole);


    Set<String> selectRoleKeyByUserId(long userId);

    List<SysRole> selectAllRoleInfoByUserId(Long userId);

    SysRole selectRoleInfoById(long roleId);

    int updateSysRoleInfo(SysRole sysRole);

    int insertSysRoleInfo(SysRole sysRole);

    int deleteSysRoleInfo(String roleIds);

    boolean checkExitByRoleInfo(SysRole sysRole);
}
