package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysDept;
import com.ljj.admin.mybatis.system.beans.SysUser;

import java.util.List;

public interface ISysUserService {
    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    SysUser selectByUserName(String userName);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKeyWithBLOBs(SysUser record);

    int updateByPrimaryKey(SysUser record);

    List<SysUser> selectNormalUserList(SysUser sysUser);

    int checkLoginNameUnique(String loginName);

    List<SysUser> selectByDeptId(Long deptId);

    List<SysUser> GetAllUserInfo(SysUser sysUser);

    int insertUserInfo(SysUser sysUser);

    int updateUserInfoByUser(SysUser sysUser);

    int delUserInfoByUser(SysUser sysUser);

    int resetUserPwdByUserId(Long userId);

}
