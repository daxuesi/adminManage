package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysRole;
import com.ljj.admin.mybatis.system.beans.SysRoleMenuKey;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysRoleMenuDAO {

    List<SysRoleMenuKey> selectMenuIdByRoleId(long roleId);

    List<SysRoleMenuKey> selectByMenuId(long menuId);

    int deleteByPrimaryKey(SysRoleMenuKey key);

    int insert(SysRoleMenuKey record);

    int insertSelective(SysRoleMenuKey record);

    int deletePrimaryKeyByRoleId(SysRole sysRole);

    int batchInsertRoleMenu(List<SysRoleMenuKey> sysRoleMenuKeys);


}