package com.ljj.admin.mybatis.system.mapper;

import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.beans.SysUserPostKey;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserPostDAO {
    int deleteByPrimaryKey(SysUserPostKey key);

    int deleteByUserId(SysUser sysUser);

    int insert(SysUserPostKey record);

    int insertSelective(SysUserPostKey record);

    List<SysUserPostKey> selectByUserId(Long userId);
}