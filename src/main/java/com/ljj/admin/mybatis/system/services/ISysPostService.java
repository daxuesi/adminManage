package com.ljj.admin.mybatis.system.services;

import com.ljj.admin.mybatis.system.beans.SysPost;
import com.ljj.admin.web.bean.common.DTree;

import java.util.List;

public interface ISysPostService {

    int deleteByPrimaryKey(Long postId);

    int insert(SysPost record);

    int insertSelective(SysPost record);

    SysPost selectByPrimaryKey(Long postId);

    int updateByPrimaryKeySelective(SysPost record);

    int updateByPrimaryKeyWithBLOBs(SysPost record);

    int updateByPrimaryKey(SysPost record);

    List<SysPost> selectAllPostInfo();

    List<SysPost> selectAllPostInfoByUserId(long userId);

    List<DTree> getPostDtree();

    List<DTree> getPostDTreeByUserId(Long userId);

    SysPost selectByPostId(Long postId);

    int insertSysPost(SysPost sysPost);

    int updateSysPost(SysPost sysPost);
}
