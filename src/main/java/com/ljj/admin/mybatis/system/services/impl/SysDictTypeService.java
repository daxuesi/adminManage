package com.ljj.admin.mybatis.system.services.impl;

import com.ljj.admin.mybatis.system.beans.SysDictType;
import com.ljj.admin.mybatis.system.mapper.SysDictTypeDAO;
import com.ljj.admin.mybatis.system.services.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysDictTypeService implements ISysDictTypeService {

    @Autowired
    private SysDictTypeDAO sysDictTypeDao;

    @Override
    public int deleteByPrimaryKey(Long dictId) {
        return sysDictTypeDao.deleteByPrimaryKey(dictId);
    }

    @Override
    public int insert(SysDictType record) {
        return sysDictTypeDao.insert(record);
    }

    @Override
    public int insertSelective(SysDictType record) {
        return sysDictTypeDao.insertSelective(record);
    }

    @Override
    public SysDictType selectByPrimaryKey(Long dictId) {
        return sysDictTypeDao.selectByPrimaryKey(dictId);
    }

    @Override
    public int updateByPrimaryKeySelective(SysDictType record) {
        return sysDictTypeDao.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysDictType record) {
        return sysDictTypeDao.updateByPrimaryKey(record);
    }

    @Override
    public List<SysDictType> selectAll() {
        return sysDictTypeDao.selectAll();
    }
}
