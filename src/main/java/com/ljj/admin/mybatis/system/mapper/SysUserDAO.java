package com.ljj.admin.mybatis.system.mapper;

import org.springframework.stereotype.Repository;
import com.ljj.admin.mybatis.system.beans.SysUser;

import java.util.List;

/**
 * SysUserDAO继承基类
 */
@Repository
public interface SysUserDAO extends MyBatisBaseDAO<SysUser, Long> {


    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    SysUser selectByUserName(String userName);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKeyWithBLOBs(SysUser record);

    int updateByPrimaryKey(SysUser record);

    List<SysUser> selectNormalUserList(SysUser sysUser);

    int checkLoginNameUnique(String loginName);

    List<SysUser> selectByDeptId(Long deptId);


}