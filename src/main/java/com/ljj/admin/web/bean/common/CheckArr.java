package com.ljj.admin.web.bean.common;

import java.util.Objects;

public class CheckArr {


    /** 复选框标记*/
    private String type="0";
    /** 复选框是否选中*/
    private String checked="0";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckArr checkArr = (CheckArr) o;
        return Objects.equals(type, checkArr.type) &&
                Objects.equals(checked, checkArr.checked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, checked);
    }
}
