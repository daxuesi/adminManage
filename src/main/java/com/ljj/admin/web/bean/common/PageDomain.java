package com.ljj.admin.web.bean.common;

import cn.hutool.core.util.StrUtil;

/**
 * 分页请求数据
 */
public class PageDomain {

    /**
     * 当前页
     */
    private final Integer pageNum;

    /**
     * 每页显示的记录数
     */
    private final Integer pageSize;

    /**
     * 排序的列
     */
    private  String orderByColumn;

    /**
     * 排序方式  desc 或者 asc
     */
    private  String isAsc;

    public static class Bulider{
        private final Integer pageNum;
        private  final Integer pageSize;
        private  String isAsc ="desc";
        private String orderByColumn="";
        public Bulider(Integer _pageNum,Integer _pageSize)
        {
            this.pageNum=_pageNum;
            this.pageSize=_pageSize;
        }

        public Bulider OrderByColom(String _orderByColumn)
        {
            orderByColumn=_orderByColumn;
            return this;
        }

        public Bulider IsAsc(String _isAsc)
        {
            isAsc=_isAsc;
            return this;
        }

        public  PageDomain bulid()
        {
            return new PageDomain(this);
        }
    }
    private PageDomain(Bulider bulider)
    {
        pageNum=bulider.pageNum;
        pageSize=bulider.pageSize;
    }


    public Integer getPageNum() {
        return pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getOrderByColumn() {
        return orderByColumn;
    }

    public void setOrderByColumn(String orderByColumn) {
        this.orderByColumn = orderByColumn;
    }

    public String getIsAsc() {
        return isAsc;
    }

    public void setIsAsc(String isAsc) {
        this.isAsc = isAsc;
    }

    public String getOrderBy() {
        if (StrUtil.isEmpty(orderByColumn)) {
            return "" ;
        }
        return StrUtil.toUnderlineCase(orderByColumn) + " " + isAsc;
    }


}
