package com.ljj.admin.web.bean.common;

import com.ljj.admin.common.constants.Constants;
import com.ljj.admin.common.utils.ServletUtils;

public class TableSupport {

    private TableSupport() {
        throw new IllegalStateException("Utility class");
    }

    public static PageDomain getPageDomain()
    {
        Integer pageNum= ServletUtils.getParameterToInt(Constants.PAGE_NUM,null);
        Integer pageSize=ServletUtils.getParameterToInt(Constants.PAGE_SIZE,null);
        String ordreByColumn=ServletUtils.getParameter(Constants.ORDER_BY_COLUMN,"");
        String orderByColumn=ServletUtils.getParameter(Constants.IS_ASC);
         return new  PageDomain.Bulider(pageNum,pageSize).OrderByColom(ordreByColumn).IsAsc(orderByColumn).bulid();
    }
}
