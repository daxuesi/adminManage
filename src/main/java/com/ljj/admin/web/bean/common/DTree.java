package com.ljj.admin.web.bean.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DTree {

    /** 节点ID*/
    private String id;

    /** 上级节点ID*/
    private String parentId;

    /** 节点名称*/
    private String title;

    /** 是否展开节点*/
    private Boolean spread;

    /** 是否最后一级节点*/
    private boolean last;
    /** 是否隐藏*/
    private Boolean hide;
    /** 是否禁用*/
    private Boolean disabled;
    /** 自定义图标class*/
    private String iconClass;
    /** 表示用户自定义需要存储在树节点中的数据*/
    private Object basicData;

    /** 复选框集合*/
    private CheckArr checkArr ;
    /** 子节点集合*/
    private List<DTree> children = new ArrayList<DTree>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getSpread() {
        return spread;
    }

    public void setSpread(Boolean spread) {
        this.spread = spread;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public Boolean getHide() {
        return hide;
    }

    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getIconClass() {
        return iconClass;
    }

    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    public Object getBasicData() {
        return basicData;
    }

    public void setBasicData(Object basicData) {
        this.basicData = basicData;
    }

    //public List<CheckArr> getCheckArr() {
    //    return checkArr;
    //}
    //
    //public void setCheckArr(List<CheckArr> checkArr) {
    //    this.checkArr = checkArr;
    //}


    public CheckArr getCheckArr() {
        return checkArr;
    }

    public void setCheckArr(CheckArr checkArr) {
        this.checkArr = checkArr;
    }

    public List<DTree> getChildren() {
        return children;
    }

    public void setChildren(List<DTree> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DTree dTree = (DTree) o;
        return last == dTree.last &&
                Objects.equals(id, dTree.id) &&
                Objects.equals(parentId, dTree.parentId) &&
                Objects.equals(title, dTree.title) &&
                Objects.equals(spread, dTree.spread) &&
                Objects.equals(hide, dTree.hide) &&
                Objects.equals(disabled, dTree.disabled) &&
                Objects.equals(iconClass, dTree.iconClass) &&
                Objects.equals(basicData, dTree.basicData) &&
                Objects.equals(checkArr, dTree.checkArr) &&
                Objects.equals(children, dTree.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, parentId, title, spread, last, hide, disabled, iconClass, basicData, checkArr, children);
    }
}
