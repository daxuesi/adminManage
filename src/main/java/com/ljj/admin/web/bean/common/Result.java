package com.ljj.admin.web.bean.common;

import com.ljj.admin.mybatis.system.beans.SysUser;

import java.io.Serializable;
import java.util.Objects;

/**
 * APi 请求返回
 * code =0 成功
 * code= 其他 失败
 * code= 404 不存在
 * code=701 格式错误
 */
public class Result<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;
    private int count;

    public Result(int code ,String msg ,int count,T data)
    {
        this.code=code;
        this.message=msg;
        this.data=data;
        this.count=count;
    }

    public Result(int code, String msg, T data) {

        this.code=code;
        this.message=msg;
        this.data=data;
        this.count=0;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Result<?> result = (Result<?>) o;
        return count == result.count &&
                Objects.equals(code, result.code) &&
                Objects.equals(message, result.message) &&
                Objects.equals(data, result.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, message, data, count);
    }
}
