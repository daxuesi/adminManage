package com.ljj.admin.web.ApiController;

import com.ljj.admin.mybatis.system.mapper.SysUserDAO;
import com.ljj.admin.web.bean.common.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ljj.admin.mybatis.system.beans.SysUser;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private SysUserDAO sysUserDAO;

    @RequestMapping(value = "/getSysInfo", method = RequestMethod.GET)
    public Result getSysInfo() {
        SysUser sysUser=sysUserDAO.selectByPrimaryKey(1L);

        return new Result(404, "页面不存在", sysUser);
    }


}
