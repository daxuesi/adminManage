package com.ljj.admin.web.controller.system;

import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/system/notice")
public class NoticeController extends BaseController {


    private  final String profix="/system/notice";

    @RequiresPermissions("system:notice:view")
    @GetMapping
    public String NoticeInfo()
    {
        return profix + "/notice";
    }


}
