package com.ljj.admin.web.controller.system;

import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/system/config")
public class ConfigController extends BaseController {

    private final String profix="/system/config/";

    @RequiresPermissions("system:config:view")
    @GetMapping
    public String ConfigInfo()
    {
        return profix +"config";
    }


}
