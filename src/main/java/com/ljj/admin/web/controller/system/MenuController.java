package com.ljj.admin.web.controller.system;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysMenu;
import com.ljj.admin.mybatis.system.beans.SysRoleMenuKey;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.services.ISysMenusService;
import com.ljj.admin.mybatis.system.services.impl.SysMenusService;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.DTree;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/system/menu")
public class MenuController extends BaseController {

    private final String profix = "/system/menu";

    @Autowired
    private ISysMenusService sysMenusService;



    @RequiresPermissions("system:menu:view")
    @GetMapping
    public String MenuInfo() {

        return profix + "/menu";
    }

    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo getMenuInfo() {
//        startPage();
        SysUser sysUser = ShiroUtils.getSysUser();
        List<SysMenu> sysMenus = sysMenusService.getMenusAll(sysUser);
        return getDataTable(sysMenus);
    }

    @PostMapping("/dTree")
    @ResponseBody
    public AjaxResult getMenuDTree(long roleId) {
        SysUser sysUser = ShiroUtils.getSysUser();

        List<DTree> dTrees = sysMenusService.GetSysMenuDTreeByUser(roleId);
        return new AjaxResult(AjaxResult.Type.SUCCESS, "成功", dTrees);
    }

    @GetMapping("/add")
    public String add(long menuId, ModelMap modelMap) {
        SysMenu sysMenu = null;

        if (0L != menuId) {
            sysMenu = sysMenusService.selectByPrimaryKey(menuId);
        } else {
            sysMenu = new SysMenu();
            sysMenu.setParentId(0l);
            sysMenu.setMenuId(0l);
            sysMenu.setMenuName("主目录");
        }
        modelMap.addAttribute("sysMenu", sysMenu);
        return profix + "/add";
    }


    @PostMapping("/addMenu")
    @ResponseBody
    public AjaxResult addMenu(SysMenu sysMenu) {
        //判断 菜单类型
        if (ObjectUtil.isNull(sysMenu)) {
            return error("新增参数为空，无法做新增操作！！！");
        }

        if(StringUtils.isEmpty(sysMenu.getMenuName()))
        {
            return error("菜单名称不能为空或包含非法标识");
        }



        switch (sysMenu.getMenuType())
        {
            case "M":

                if(StringUtils.isEmpty(sysMenu.getUrl()))
                {
                    return error("菜单名称不能为空或包含非法标识");
                }

                if(StringUtils.isEmpty(sysMenu.getPerms()))
                {
                    return error("菜单名称不能为空或包含非法标识");
                }
                sysMenu.setUrl("#");
                sysMenu.setPerms("");
                break;
            case "C":
                break;
            case "F":
                sysMenu.setUrl("#");
                break;
            default:
                return error("菜单类型不存在！！");
        }
        sysMenu.setCreateBy(ShiroUtils.getSysUser().getLoginName());
        sysMenu.setCreateTime(DateTime.now());
        sysMenu.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        sysMenu.setUpdateTime(DateTime.now());

        // 做菜单新增操作
        return toAjax(sysMenusService.insertSelective(sysMenu));
    }


    @PostMapping("/checkMenuNameUnique")
    @ResponseBody
    public AjaxResult checkMenuNameUnique(SysMenu sysMenu) {

        return success("菜单名称已存在");
    }


    /**
     * 返回菜单编辑页面
     * @param menuId
     * @param modelMap
     * @return
     */
    @GetMapping("/edit")
    public String edit(long menuId,ModelMap modelMap) {
        SysMenu sysMenu = sysMenusService.selectByPrimaryKey(menuId);
        if(0l==sysMenu.getParentId())
        {
            sysMenu.setParentName("主目录");
        }
        modelMap.addAttribute("sysMenu",sysMenu);
        return profix + "/edit";
    }


    @PostMapping("/editMenu")
    @ResponseBody
    public AjaxResult editMenu(SysMenu sysMenu)
    {

        sysMenu.setUpdateTime(DateTime.now());
        sysMenu.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        return toAjax(sysMenusService.updateByPrimaryKeySelective(sysMenu));
    }


    @PostMapping("/del")
    @ResponseBody
    public AjaxResult removeMenu(long menuId)
    {
        if(StringUtils.isEmpty(menuId))
        {
            return  error("无法获取要删除的菜单信息ID");
        }
        //查询是否存在子菜单

        List<SysMenu> sysMenus = sysMenusService.selectByParentId(menuId);
        if(CollectionUtil.isNotEmpty(sysMenus))
        {
            if(sysMenus.size()>0)
            {
                return error("还有未删除的子菜单，无法执行删除操作");
            }
        }

        List<SysRoleMenuKey> sysRoleMenuKeys = sysMenusService.selectByMenuId(menuId);
        if(CollectionUtil.isNotEmpty(sysRoleMenuKeys))
        {
            if(sysRoleMenuKeys.size()>0)
            {
                return error("菜单已被分配，不允许删除");            }
        }


        return toAjax(sysMenusService.deleteByPrimaryKey(menuId));
    }





}