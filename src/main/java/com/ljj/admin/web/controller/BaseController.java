package com.ljj.admin.web.controller;

import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.common.utils.SqlUtil;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.PageDomain;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.bean.common.TableSupport;
import org.springframework.util.ObjectUtils;

import java.util.List;

public class BaseController {

    /**
     * 返回成功
     * @return
     */
    public AjaxResult success()
    {
        return AjaxResult.success();
    }

    /**
     * 返回成功消息
     * @param msg
     * @return
     */
    public AjaxResult success(String msg)
    {
        return AjaxResult.success(msg);
    }

    public AjaxResult success(Object data)
    {
        return AjaxResult.success(data);
    }

    /**
     * 返回失败
     * @return
     */
    public AjaxResult error()
    {
        return AjaxResult.error();
    }

    /**
     * 返回失败消息
     * @param msg
     * @return
     */
    public AjaxResult error(String msg)
    {
        return AjaxResult.error(msg);
    }



    /**
     * 获取当前用户信息
     * @return
     */
    public SysUser getSysUser()
    {
        return ShiroUtils.getSysUser();
    }



    /**
     * 设置分页请求
     */
    protected void startPage(){
        Integer pageNum=TableSupport.getPageDomain().getPageNum();
        Integer pageSize=TableSupport.getPageDomain().getPageSize();
        if(ObjectUtil.isAllNotEmpty(pageNum,pageSize))
        {
            String orderBy = SqlUtil.escapeOrderBySql(TableSupport.getPageDomain().getOrderBy());
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }


    /**
     * 表数据信息组装
     * @param list
     * @return
     */
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(0);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }


    protected AjaxResult toAjax(int rows)
    {
        return rows > 0 ? success() : error();
    }


}
