package com.ljj.admin.web.controller.system;

import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysDept;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.services.ISysDeptService;
import com.ljj.admin.mybatis.system.services.impl.SysDeptService;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.DTree;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/system/dept")
public class DeptController extends BaseController {

    private final String profix = "/system/dept/";

    @Autowired
    private ISysDeptService sysDeptService;

    @RequiresPermissions("system:dept:view")
    @GetMapping
    public String DeptInfo() {
        return profix + "dept";
    }

    @PostMapping("/detpDtree")
    @ResponseBody
    public AjaxResult getDeptDtreeList() {
        List<DTree> deptDtree = sysDeptService.getDeptDtree();
        return new AjaxResult(AjaxResult.Type.SUCCESS, "加载成功", deptDtree);
    }


    /**
     * 返回部门列表
     *
     * @return
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo GetDeptList() {
        List<SysDept> deptList = sysDeptService.getDeptListAll();
        return getDataTable(deptList);
    }


    /**
     * 返回新增界面
     *
     * @return
     */
    @GetMapping("addPage")
    public String GetAddPage(Long deptId, ModelMap mmap) {
        SysDept sysDept = null;

        if (0L != deptId.intValue()) {
            sysDept = sysDeptService.selectByPrimaryKey(deptId);
        }else
        {
            // 数据库中 id 100 的部门为根节点 所以这里 用100 查询 根节点信息
            sysDept = sysDeptService.selectByPrimaryKey(100L);
        }
        mmap.addAttribute("sysDept",sysDept);

        return profix + "add";
    }


    /**
     * 部门信息新增
     * @param sysDept
     * @return
     */
    @PostMapping("insert")
    @ResponseBody
    public AjaxResult insert(SysDept sysDept)
    {
        if(ObjectUtil.isNull(sysDept))
        {
            return error("部门信息为空，无法完成新增操作");
        }

        if(StringUtils.isEmpty(sysDept.getParentId()))
        {
            return error("上级部门信息为空，无法完成新增操作");
        }
        // 数据赋值
        sysDept.setCreateBy(ShiroUtils.getSysUser().getLoginName());
        sysDept.setUpdateBy(ShiroUtils.getSysUser().getLoginName());

        return toAjax(sysDeptService.insertDept(sysDept));
    }


    /**
     * 返回新增界面
     *
     * @return
     */
    @GetMapping("editPage")
    public String GetEditPage(Long deptId, ModelMap mmap) {
        SysDept sysDept = sysDeptService.selectByPrimaryKey(deptId);
        //获取该部门上级部门信息
        SysDept parentDept = sysDeptService.selectByPrimaryKey(sysDept.getParentId());
        mmap.addAttribute("sysDept",sysDept);
        mmap.addAttribute("parentDept",parentDept);
        return profix + "edit";
    }


    /****
     * 部门信息 修改
     * @param sysDept
     * @return
     */
    @PostMapping("update")
    @ResponseBody
    public AjaxResult update(SysDept sysDept)
    {
        if(ObjectUtil.isNull(sysDept))
        {
            return error("部门信息为空，无法执行修改操作");
        }
        if(StringUtils.isEmpty(sysDept.getParentId()))
        {
            return error("上级部门信息为空，无法修改");
        }
        sysDept.setUpdateBy(ShiroUtils.getSysUser().getLoginName());

        return toAjax(sysDeptService.updateDept(sysDept));
    }


    /**
     * 根据Id 删除 部门信息
     * 树形结构 ，不支持批量删除
     * @return
     */
    @PostMapping("del")
    @ResponseBody
    public AjaxResult del(long deptId)
    {
        if(StringUtils.isEmpty(deptId))
        {
            return error("部门信息为空，无法做删除操作");
        }
        // 删除操作注意事项
        // 判断 是否还有下级部门
        List<SysDept> byParentIdList = sysDeptService.getSysDeptListByParentId(deptId);
        if(byParentIdList.size()>0)
        {
            return error("还有下级部门信息，无法删除");
        }
        // 部门还有用户信息没
        List<SysUser> sysUserInfoByDeptId = sysDeptService.getSysUserInfoByDeptId(deptId);
        if(sysUserInfoByDeptId.size()>0)
        {
            return error("部门还存在用户信息，无法删除");
        }


        return toAjax(sysDeptService.delByDeptId(deptId));
    }








    /**
     * 部门名称校验
     * @return
     */
    @PostMapping("deptNameModify")
    @ResponseBody
    public AjaxResult deptNameModify()
    {
        return success("部门名称重复");
    }




}
