package com.ljj.admin.web.controller.system;

import com.ljj.admin.mybatis.system.beans.SysDictType;
import com.ljj.admin.mybatis.system.mapper.SysDictTypeDAO;
import com.ljj.admin.mybatis.system.services.ISysDictTypeService;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/system/dict")
public class DictController extends BaseController {

    private final String profix = "/system/dict";

    @Autowired
    private ISysDictTypeService sysDictTypeService;

    @Autowired
    private SysDictTypeDAO sysDictTypeDao;

    @RequiresPermissions("system:dict:view")
    @GetMapping
    public String DictInfo() {
        return profix + "/dict";
    }


    /**
     * 新增界面
     *
     * @return
     */
    @RequiresPermissions("system:dict:add")
    @GetMapping("/add")
    public String DictAdd() {
        return profix + "/add";
    }


    /**
     * 编辑界面
     *
     * @return
     */
    @RequiresPermissions("system:dict:edit")
    @GetMapping("/edit")
    public String DictEdit(@RequestParam("dictId") Long dictId) {
        return profix + "/edit";
    }


    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo getDictTypeList() {
        startPage();
        List<SysDictType> sysDictTypes = sysDictTypeDao.selectAll();
        return getDataTable(sysDictTypes);
    }

}
