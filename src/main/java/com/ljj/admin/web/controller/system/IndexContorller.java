package com.ljj.admin.web.controller.system;

import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysMenu;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.services.ISysMenusService;
import com.ljj.admin.mybatis.system.services.impl.SysMenusService;
import com.ljj.admin.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexContorller extends BaseController {

    @Autowired
    private ISysMenusService sysMenusService;



    @GetMapping("/index")
    public String index(ModelMap mmap)
    {
        SysUser sysUser = ShiroUtils.getSysUser();
        List<SysMenu> menuList =sysMenusService.GetSysMenuListByUser(sysUser);
        mmap.addAttribute("menuList",menuList);
        mmap.addAttribute("sysUser",sysUser);
        return "index";
    }

    @GetMapping("/main")
    public  String main(ModelMap mmap)
    {

        return "main";
    }



}
