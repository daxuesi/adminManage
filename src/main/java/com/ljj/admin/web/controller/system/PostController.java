package com.ljj.admin.web.controller.system;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysPost;
import com.ljj.admin.mybatis.system.services.ISysPostService;
import com.ljj.admin.mybatis.system.services.impl.SysPostService;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.DTree;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/post")
public class PostController extends BaseController {

    private final String profix="/system/post/";

    @Autowired
    private ISysPostService sysPostService;


    @RequiresPermissions("system:post:view")
    @GetMapping
    public String PostInfo()
    {
        return profix+ "post";
    }

    @PostMapping("/postdtree")
    @ResponseBody
    public Map getPostDTree()
    {
        List<DTree> postDtree = sysPostService.getPostDtree();
        Map map=new HashMap();
        map.put("code",0);
        map.put("msg","操作成功");
        map.put("data",postDtree);
        return map;
    }


    /**
     * 根据用户Id 获取 岗位 树数据
     * @return
     */
    @PostMapping("/userPostDTree")
    @ResponseBody
    public Map getPostDTreeByUserId(Long userId)
    {
        Map map=new HashMap();
        List<DTree> postDtree = sysPostService.getPostDTreeByUserId(userId);
        map.put("code",0);
        map.put("msg","操作成功");
        map.put("data",postDtree);
        return map;
    }

    /**
     * 去所有数据
     * 数据量太少 不做分页处理
     * @return
     */
    @PostMapping("/postList")
    @ResponseBody
    public TableDataInfo postList()
    {
        List<SysPost> sysPosts = sysPostService.selectAllPostInfo();
        return getDataTable(sysPosts);
    }


    /**
     * 返回新增岗位信息页面
     * @return
     */
    @GetMapping("addPage")
    public String addPage()
    {
        return profix +"add";
    }

    /**
     * 返回新增岗位信息页面
     * @return
     */
    @GetMapping("editPage")
    public String editPage(Long postId, ModelMap modelMap)
    {
        SysPost sysPost = sysPostService.selectByPostId(postId);
        modelMap.addAttribute("sysPost",sysPost);
        return profix +"edit";
    }


    /**
     * 岗位新增
     * @param sysPost
     * @return
     */
    @PostMapping("insert")
    @ResponseBody
    public AjaxResult insert(SysPost sysPost)
    {
        // 做数据入库操作
        sysPost.setCreateBy(ShiroUtils.getSysUser().getLoginName());
        sysPost.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        return toAjax(sysPostService.insertSysPost(sysPost));
    }


    /**
     * 岗位信息修改
     * @param sysPost
     * @return
     */
    @PostMapping("update")
    @ResponseBody
    public AjaxResult update(SysPost sysPost)
    {
        sysPost.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        return toAjax(sysPostService.updateSysPost(sysPost));
    }


    /**
     * 岗位状态变更
     * @param status
     * @return
     */
    @PostMapping("status")
    @ResponseBody
    public AjaxResult status(Long postId,Boolean status)
    {
        SysPost sysPost = sysPostService.selectByPostId(postId);
        if(ObjectUtil.isNull(sysPost))
        {
            return error("无法获取岗位信息，岗位状态变更失败");
        }
        if (status)
        {
            sysPost.setStatus("0");
        }else {
            sysPost.setStatus("1");
        }
        sysPost.setUpdateBy(ShiroUtils.getSysUser().getLoginName());
        sysPost.setUpdateTime(DateTime.now());
        return toAjax(sysPostService.updateSysPost(sysPost));
    }







}
