package com.ljj.admin.web.controller.system;

import cn.hutool.core.date.DateTime;
import com.ljj.admin.common.utils.ShiroUtils;
import com.ljj.admin.mybatis.system.beans.SysRole;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.services.ISysRoleService;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/system/role")
public class RoleController extends BaseController {

    private final String profix = "/system/role/";

    @Autowired
    private ISysRoleService sysRoleService;

    @RequiresPermissions("system:role:view")
    @GetMapping
    public String RoleInfo() {
        return profix + "role";
    }

    @GetMapping("/addRole")
    public String AddRoleInfo() {
        return profix + "addRole";
    }

    @PostMapping("/rolelist")
    @ResponseBody
    public TableDataInfo RoleList() {
        startPage();
        List<SysRole> sysRoles = sysRoleService.selectAllRoleInfo();
        return getDataTable(sysRoles);
    }

    @GetMapping("/edit")
    public String roleEdit(long roleId, ModelMap mmap) {
        SysRole sysRole = sysRoleService.selectRoleInfoById(roleId);
        mmap.addAttribute("sysRole", sysRole);
        return profix + "edit";
    }


    @PostMapping("/update")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult updateRole(SysRole sysRole) {
        // 修改信息保存
        SysUser sysUser = ShiroUtils.getSysUser();
        sysRole.setUpdateBy(sysUser.getUserName());
        return toAjax(sysRoleService.updateSysRoleInfo(sysRole));
    }


    /**
     * 角色新增
     *
     * @param sysRole
     * @return
     */
    @PostMapping("/insert")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult insertRole(SysRole sysRole) {
        // 角色信息新增
        SysUser sysUser = ShiroUtils.getSysUser();
        sysRole.setUpdateBy(sysUser.getUserName());
        sysRole.setCreateBy(sysUser.getLoginName());
        sysRole.setCreateTime(DateTime.now());
        sysRole.setUpdateTime(DateTime.now());
        sysRole.setDelFlag("0");
        sysRole.setDataScope("2");
        return toAjax(sysRoleService.insertSysRoleInfo(sysRole));
    }


    @PostMapping("/delRoles")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult delRoles(String roles) {

        if(StringUtils.isEmpty(roles))
        {
            return error("角色ID为空，无法删除");
        }

        // 删除 用户角色信息
        return toAjax(sysRoleService.deleteSysRoleInfo(roles));
    }


    /**
     * 角色名称校验
     *
     * @return
     */
    @PostMapping("/roleNameModify")
    @ResponseBody
    public AjaxResult RoleNameModify(SysRole sysRole) {
        if (sysRoleService.checkExitByRoleInfo(sysRole)) {
            return error("角色名称已存在！！！");
        }
        return success();
    }


    /**
     * 角色字符校验
     *
     * @param sysRole
     * @return
     */
    @PostMapping("/roleKeyModify")
    @ResponseBody
    public AjaxResult RoleKeyModify(SysRole sysRole) {
        if (sysRoleService.checkExitByRoleInfo(sysRole)) {
            return error("角色字符已存在");
        }
        return success();
    }


}
