package com.ljj.admin.web.controller.system;

import com.ljj.admin.annotations.Log;
import com.ljj.admin.common.enums.BusinessTypeEnum;
import com.ljj.admin.mybatis.system.beans.SysDept;
import com.ljj.admin.mybatis.system.beans.SysPost;
import com.ljj.admin.mybatis.system.beans.SysRole;
import com.ljj.admin.mybatis.system.beans.SysUser;
import com.ljj.admin.mybatis.system.services.ISysDeptService;
import com.ljj.admin.mybatis.system.services.ISysPostService;
import com.ljj.admin.mybatis.system.services.ISysRoleService;
import com.ljj.admin.mybatis.system.services.ISysUserService;
import com.ljj.admin.shiro.services.UserPasswordSecureService;
import com.ljj.admin.web.bean.common.AjaxResult;
import com.ljj.admin.web.bean.common.TableDataInfo;
import com.ljj.admin.web.controller.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/system/user")
public class UserController extends BaseController {

    private final String prefix = "/system/user";

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysRoleService sysRoleService;

    @Autowired
    private ISysPostService sysPostService;

    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private UserPasswordSecureService userPasswordSecureService;



    @RequiresPermissions("system:user:view")
    @GetMapping()
    public String UserInfo() {
        return prefix + "/user";
    }

    @Log(value = "获取用户列表操作", businessType = BusinessTypeEnum.OTHER)
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo UserList(SysUser sysUser) {
        startPage();
        List<SysUser> sysUsers = sysUserService.GetAllUserInfo(sysUser);
        return getDataTable(sysUsers);
    }


    @GetMapping("/adduser")
    public String AddUser(ModelMap mmap) {
        // 获取所有的 角色信息
        List<SysRole> sysRoles = sysRoleService.selectAllRoleInfo();
        List<SysPost> sysPosts = sysPostService.selectAllPostInfo();
        mmap.addAttribute("sysRolesInfo", sysRoles);
        mmap.addAttribute("sysPostsInfo", sysPosts);
        return prefix + "/adduser";
    }

    /**
     * 新增用户  有异常 回滚
     *
     * @param user
     * @return
     */
    @PostMapping("/adduser")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult AddUserInfo(SysUser user) {
        //新增用户逻辑判断 如果已存在用户信息 则不允许新增用户返回提示
        if (sysUserService.checkLoginNameUnique(user.getLoginName()) > 0) {
            return error("保存用户失败：" + user.getUserName() + ";原因：账号已存在");
        }

        //用户信息保存密码加密
        user=userPasswordSecureService.getEncryptPasswordUserInfo(user);

        // 用户信息入库
        return toAjax(sysUserService.insertUserInfo(user));
    }


    @GetMapping("/edituser")
    public String EditUser(String loginName, ModelMap mmap) {
        // 根据用户登录名 获取用户信息
        SysUser sysUser = sysUserService.selectByUserName(loginName);


        List<SysRole> sysRoles = sysRoleService.selectAllRoleInfoByUserId(sysUser.getUserId());
        List<SysPost> sysPosts = sysPostService.selectAllPostInfoByUserId(sysUser.getUserId());

        SysDept sysDept = sysDeptService.selectByPrimaryKey(sysUser.getDeptId());


        mmap.addAttribute("sysRolesInfo", sysRoles);
        mmap.addAttribute("sysPostsInfo", sysPosts);
        mmap.addAttribute("sysuserInfo", sysUser);
        mmap.addAttribute("sysDept", sysDept);

        return prefix + "/edituser";
    }


    /**
     * 编辑用户
     *
     * @param user
     * @return
     */
    @PostMapping("/edituser")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult EditUserInfo(SysUser user) {

        return toAjax(sysUserService.updateUserInfoByUser(user));
    }


    /**
     * 删除用户
     *
     * @return
     */
    @PostMapping("/delUser")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult DelUserInfo(String loginName) {
        //根据 loginName 获取用户信息
        SysUser sysUser = sysUserService.selectByUserName(loginName);
        return toAjax(sysUserService.delUserInfoByUser(sysUser));
    }


    /**
     * 批量删除数据
     * @param ids
     * @return
     */
    @PostMapping("/remove")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult remove(String ids) {

        if (StringUtils.isEmpty(ids)) {
            return error("无法获取要删除的用户信息，删除失败");
        }
        String[] userids = ids.split(",");

        int rowNo = -1;
        for (String id : userids) {

            long userIdL = Long.parseLong(id);
            SysUser sysUser = sysUserService.selectByPrimaryKey(userIdL);
            rowNo += sysUserService.delUserInfoByUser(sysUser);
        }
        return toAjax(rowNo);
    }


    /**
     * 重置用户密码
     * @return
     */
    @PostMapping("/reset")
    @ResponseBody
    public AjaxResult resetPassword(Long uId)
    {
        int rowNum = sysUserService.resetUserPwdByUserId(uId);
        if(rowNum>0)
        {
            return success("密码重置成功，默认密码为 123 ");

        }else
        {
            return error("重置失败");
        }
    }













}
