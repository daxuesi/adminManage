# 后台管理系统

### 介绍
自己动手搭建的一套通用后台管理系统，前端框架使用的是layuiAdmin后台管理模板；后台是基于springBoot+springMVC+Shiro+Mybatis开发系统架构；
目前已基本实现用户权限控制；部门、菜单、岗位等信息的管理；通过redis缓存用户登录后的权限信息和session等信息。所以项目启动前需保证
redis 服务运行正常；

####已实现的内置功能
1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3. 岗位管理：配置系统用户所属担任职务。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。



#### 使用说明

1.  数据库执行脚本在项目sql 目录下；数据库默认mysql；系统连接数据库名称为admin ;
2.  下载项目后，通过idea 打开项目文件，启动即可
3.  登录用户名为admin 密码为 123

#### 演示图

![](https://gitee.com/daxuesi/admin-image/raw/master/20201204/admin_main.png)
![](https://gitee.com/daxuesi/admin-image/raw/master/20201204/admin_menu.png)
![](https://gitee.com/daxuesi/admin-image/raw/master/20201204/admin_post.png)
![](https://gitee.com/daxuesi/admin-image/raw/master/20201204/admin_role.png)
![](https://gitee.com/daxuesi/admin-image/raw/master/20201204/admin_user.png)



